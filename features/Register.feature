Feature: Zadanie test e2e

  Scenario: Register an account
    When I open website to register
    Then I check if website loaded
    And I fill the register form
    Then I click the button Register

  Scenario: Init an account
    Given I open login page to store
    Then I enter valid credentials
    When I am redirect to init an account
    Then I fill the details form - name
    Then I fill the details form - phone number
    Then I fill the details form - origin
    Then I fill the details form - own products
    Then I fill the details form - why testing
    Then I fill the details form - you know us
    And I click button go to shop

  Scenario: Logout form shop
    Given I see the main page
    Then I logout
    And I can see the title of login page

  Scenario: Login with invalid credentials
    When I open login page to store
    Then I can see the title of login page
    And I enter invalid credentials
    And  I see error notification

  Scenario: Login with valid credentials
    When I open login page to store
    Then I can see the title of login page
    Then I enter valid credentials
    And I see the main page

  Scenario: Create a product
    Given I see the main page
    When I click button product
    Then A window to add product displays
    And I fill the required fields
    Then I click button save
    And Notification with success appear
    Then I click button to go to list page
    And I click to select the item
    Then I delete the item
    And Notification with delete success appear
    Then I logout
    And I can see the title of login page
