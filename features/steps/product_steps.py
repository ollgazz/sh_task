#!/usr/bin/env python
# -*- coding: cp1250 -*-
from behave import *
from features.pages import *


window_title = 'Dodaj nowy produkt'
notification_success = 'Produkt zosta� dodany'
notification_delete_success = 'Nie posiadasz jeszcze �adnych produkt�w'


@step('I click button product')
def step_impl(context):
    page = ProductPage(context)
    page.click_button_product()


@step('A window to add product displays')
def step_impl(context):
    page = ProductPage(context)
    assert page.get_window_title() == window_title


@step('I fill the required fields')
def step_impl(context):
    page = ProductPage(context)
    page.enter_required_fields(context.config.userdata['name_product'], context.config.userdata['sku'], context.config.userdata['price'])


@step('I click button save')
def step_impl(context):
    page = ProductPage(context)
    page.click_button_save()


@step('Notification with success appear')
def step_impl(context):
    page = ProductPage(context)
    notification = page.get_text_notification_success()
    assert notification == notification_success


@step('I click button to go to list page')
def step_impl(context):
    page = ProductPage(context)
    page.click_back_to_product_list()


@step('I click to select the item')
def step_impl(context):
    page = ProductPage(context)
    page.click_select_produckt()


@step('I delete the item')
def step_impl(context):
    page = ProductPage(context)
    page.click_dropdown_action(7)
    page.click_button_save_action()


@step('Notification with delete success appear')
def step_impl(context):
    page = ProductPage(context)
    notification = page.get_text_when_no_products()
    assert notification == notification_delete_success
