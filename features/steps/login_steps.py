#!/usr/bin/env python
# -*- coding: cp1250 -*-

from behave import *
from features.pages import *


err = 'Niepoprawny adres email lub has�o'
header = ', witaj w Shoplo!'
desc = 'Zaloguj si� na swoje konto poni�ej:'

@step('I open login page to store')
def step_impl(context):
    page = RegisterPage(context)
    url = page.url_for_store(context.config.userdata['store_name'])
    page.visit(url)


@step('I can see the title of login page')
def step_impl(context):
    page = LoginPage(context)
    assert page.get_text_desc() == desc


@step('I enter invalid credentials')
def step_impl(context):
    page = LoginPage(context)
    page.login(context.config.userdata['invalid_mail'], context.config.userdata['invalid_pass'])


@step('I see error notification')
def step_impl(context):
    page = LoginPage(context)
    assert page.get_notification_error() == err


@step('I enter valid credentials')
def step_impl(context):
    page = LoginPage(context)
    page.login(context.config.userdata['mail'], context.config.userdata['pass'])


@step('I see the main page')
def step_impl(context):
    page = LoginPage(context)
    assert page.get_text_header() == context.config.userdata['user'] + header


@step('I logout')
def step_impl(context):
    page = LoginPage(context)
    page.logout()
