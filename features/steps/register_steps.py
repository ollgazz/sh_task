#!/usr/bin/env python
# -*- coding: cp1250 -*-
from behave import *
from features.pages import *
import time
title = 'Wypr�buj za darmo\nprzez 15 dni.'
title_text = 'Bez zobowi�za�, mo�esz zrezygnowa� w ka�dej chwili.'
init_title_text = 'Odpowiedz na kilka kr�tkich pyta�,\na my dopasujemy Shoplo do Twoich potrzeb:'


# @step('I open website to init an account')
# def step_impl(context):
#     page = RegisterPage(context)
#     page.visit(page.url_for_store(context.config.userdata['store_name']))


@step('I open website to register')
def step_impl(context):
    page = RegisterPage(context)
    page.visit(context.config.userdata['register_url'])


@step('I check if website loaded')
def step_impl(context):
    page = RegisterPage(context)
    assert page.get_title() == title
    assert page.get_text() == title_text


@step('I fill the register form')
def step_impl(context):
    page = RegisterPage(context)
    page.register(context.config.userdata['store_name'], context.config.userdata['mail'], context.config.userdata['pass'])


@step('I click the button Register')
def step_impl(context):
    page = RegisterPage(context)
    page.click_register_store()



@step('I am redirect to init an account')
def step_impl(context):
    page = DetailsRegisterPage(context)
    assert page.get_title_text() == init_title_text


@step('I fill the details form - name')
def step_impl(context):
    page = DetailsRegisterPage(context)
    page.enter_user_name(context.config.userdata['user'])


@step('I fill the details form - phone number')
def step_impl(context):
    page = DetailsRegisterPage(context)
    page.enter_phone_number(context.config.userdata['phone'])


@step('I fill the details form - origin')
def step_impl(context):
    page = DetailsRegisterPage(context)
    page.click_dropdown(0)
    page.choose_from_list(0, 1)


@step('I fill the details form - own products')
def step_impl(context):
    page = DetailsRegisterPage(context)
    page.click_dropdown(1)
    page.choose_from_list(1, 3)


@step('I fill the details form - why testing')
def step_impl(context):
    page = DetailsRegisterPage(context)
    page.click_dropdown(2)
    page.choose_from_list(1, 2)


@step('I fill the details form - you know us')
def step_impl(context):
    page = DetailsRegisterPage(context)
    page.click_dropdown(3)
    page.choose_from_list(1, 6)



@step('I click button go to shop')
def step_impl(context):
    page = DetailsRegisterPage(context)
    page.click_go_to_store()

