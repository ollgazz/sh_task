from selenium.webdriver.common.by import By


class BasePageLocators(object):

    LOGOUT = (By.CSS_SELECTOR, ".dropdown-menu.dropdown-menu-with-icon li a")
    HEADER_USER = (By.CSS_SELECTOR, '#headerUser button[data-toggle="dropdown"]')
    HEADER = (By.CSS_SELECTOR, '.connect-list-header h1')


class BasePage(object):

    def __init__(self, browser):
        self.browser = browser
        self.timeout = 10

    def find_element(self, *loc):
        return self.browser.find_element(*loc)

    def find_elements(self, *loc):
        return self.browser.find_elements(*loc)

    def visit(self, url):
        self.browser.get(url)

    def get_text_header(self):
        return self.find_element(*BasePageLocators.HEADER).text

    def logout(self):
        self.find_element(*BasePageLocators.HEADER_USER).click()
        self.find_elements(*BasePageLocators.LOGOUT)[5].click()

    def url_for_store(self, name_store):
        url = 'https://' + name_store + '.shoplo.com/admin/'
        return url



    


