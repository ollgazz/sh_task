from selenium.webdriver.common.by import By
from features.pages.base_page_object import BasePage


class RegisterPageLocators(object):
    INPUT_STORE_NAME = (By.ID, "storeName")
    INPUT_MAIL = (By.ID, "storeEmail")
    INPUT_PASSWORD = (By.ID, "storePassword")
    BUTTON_BEGIN_TESTS = (By.CSS_SELECTOR, ".btn.btn-large.btn-purple.btn-block")
    TEXT_TITLE = (By.CSS_SELECTOR, "h2")
    TEXT_SMALL = (By.CSS_SELECTOR, ".login-form p")


class RegisterPage(BasePage):

    def __init__(self, context):
        BasePage.__init__(self, context.browser)

    def enter_store_name(self, username):
        self.find_element(*RegisterPageLocators.INPUT_STORE_NAME).clear()
        self.find_element(*RegisterPageLocators.INPUT_STORE_NAME).send_keys(username)

    def enter_mail(self, username):
        self.find_element(*RegisterPageLocators.INPUT_MAIL).clear()
        self.find_element(*RegisterPageLocators.INPUT_MAIL).send_keys(username)

    def enter_password(self, passwd):
        self.find_element(*RegisterPageLocators.INPUT_PASSWORD).clear()
        self.find_element(*RegisterPageLocators.INPUT_PASSWORD).send_keys(passwd)

    def click_register_store(self):
        return self.find_element(*RegisterPageLocators.BUTTON_BEGIN_TESTS).click()

    def register(self, store_name, mail, passwd):
        self.enter_store_name(store_name)
        self.enter_mail(mail)
        self.enter_password(passwd)

    def get_title(self):
        return self.find_element(*RegisterPageLocators.TEXT_TITLE).text

    def get_text(self):
        return self.find_element(*RegisterPageLocators.TEXT_SMALL).text








