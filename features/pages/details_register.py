#!/usr/bin/env python
#-*- coding: utf-8 -*-
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from features.pages.base_page_object import BasePage


class DetailsRegisterPageLocators(object):
    INPUT_USER_NAME = (By.CSS_SELECTOR, 'input[name="name"]')
    INPUT_PHONE_NUMBER = (By.CSS_SELECTOR, 'input[name="phone"]')
    TEXT_TITLE = (By.CSS_SELECTOR, '.welcome-form p')
    BUTTON_GO_TO_STORE = (By.CSS_SELECTOR, '.btn.btn-large.btn-purple.btn-block')
    DROPDOWN_COUNTRY = (By.CSS_SELECTOR, '.btn-group.bootstrap-select.-flags.show')
    DROPDOWNS = (By.CSS_SELECTOR, '.btn-group.bootstrap-select.-flags')
    DROPDOWNS_LIST = (By.CSS_SELECTOR, '.dropdown-menu.show')


class DetailsRegisterPage(BasePage):

    def __init__(self, context):
        BasePage.__init__(self, context.browser)

    def enter_user_name(self, username):
        self.find_element(*DetailsRegisterPageLocators.INPUT_USER_NAME).clear()
        self.find_element(*DetailsRegisterPageLocators.INPUT_USER_NAME).send_keys(username)

    def enter_phone_number(self, phone):
        self.find_element(*DetailsRegisterPageLocators.INPUT_PHONE_NUMBER).clear()
        self.find_element(*DetailsRegisterPageLocators.INPUT_PHONE_NUMBER).send_keys(phone)

    def click_dropdown(self, num):
        self.find_elements(*DetailsRegisterPageLocators.DROPDOWNS)[num].click()

    def choose_from_list(self, num, row):
        list = self.find_elements(*DetailsRegisterPageLocators.DROPDOWNS_LIST)[num]
        list.find_elements_by_css_selector('.dropdown-menu.inner li')[row].click()

    def click_go_to_store(self):
        return self.find_element(*DetailsRegisterPageLocators.BUTTON_GO_TO_STORE).click()

    def get_title_text(self):
        WebDriverWait(self.browser, self.timeout).until(EC.visibility_of_element_located((By.CSS_SELECTOR, '.welcome-form p')))
        return self.find_element(*DetailsRegisterPageLocators.TEXT_TITLE).text









