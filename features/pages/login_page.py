from selenium.webdriver.common.by import By
from features.pages.base_page_object import BasePage


class LoginPageLocators(object):
    INPUT_USERNAME = (By.CSS_SELECTOR, 'input[name="email"]')
    INPUT_PASSWORD = (By.CSS_SELECTOR, 'input[name="password"]')
    BUTTON_LOGIN = (By.CSS_SELECTOR, ".btn.btn-large.btn-purple.btn-block")
    NOTIFICATION_ERROR = (By.CSS_SELECTOR, ".notification.notification-error")
    TEXT_SMALL_DESC = (By.CSS_SELECTOR, "h5")


class LoginPage(BasePage):

    def __init__(self, context):
        BasePage.__init__(self, context.browser)

    def enter_mail(self, username):
        self.find_element(*LoginPageLocators.INPUT_USERNAME).clear()
        self.find_element(*LoginPageLocators.INPUT_USERNAME).send_keys(username)

    def enter_password(self, passwd):
        self.find_element(*LoginPageLocators.INPUT_PASSWORD).clear()
        self.find_element(*LoginPageLocators.INPUT_PASSWORD).send_keys(passwd)

    def click_login_button(self):
        return self.find_element(*LoginPageLocators.BUTTON_LOGIN).click()

    def get_notification_error(self):
        return self.find_element(*LoginPageLocators.NOTIFICATION_ERROR).text

    def login(self, email, passwd):
        self.enter_mail(email)
        self.enter_password(passwd)
        self.click_login_button()

    def get_text_desc(self):
        return self.find_element(*LoginPageLocators.TEXT_SMALL_DESC).text








