from features.pages.base_page_object import BasePage
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
import time


class ProductPageLocators(object):
    BUTTON_PRODUCT_MAINPAGE = (By.CSS_SELECTOR, ".btn.btn-small.btn-white.btn-with-icon")
    BUTTON_SAVE = (By.CSS_SELECTOR, ".buttons-group")
    BUTTON_BACK_TO_PRODUCTS_LIST =(By.CSS_SELECTOR, ".btn.btn-small.btn-with-icon.btn-white")
    BUTTON_SAVE_ACTION = (By.CSS_SELECTOR, ".btn.btn-blue.pull-left")
    DROPDOWN_ACTIONS = (By.CSS_SELECTOR, 'button[class="dropdown-toggle bs-placeholder btn btn-base btn-small"]')
    DROPDOWN_MENU = (By.CSS_SELECTOR, 'div[class="dropdown-menu show"] li')
    WINDOW_ADD_PRODUCT = (By.CSS_SELECTOR, "#add_product_form h2")
    INPUT_PRODUCT_NAME = (By.ID, "name")
    INPUT_CATALOG_NUMBER = (By.ID, "sku")
    INPUT_PRODUCT_PRICE = (By.CSS_SELECTOR, 'input[name="price"]')
    NOTIFICATION_SUCCESS = (By.CSS_SELECTOR, '#panelNotification li')
    LISTS_OF_PRODUCTS = (By.CSS_SELECTOR, '.items-list.sort')
    CHECKBOX_PRODUCKT = (By.CSS_SELECTOR, '.items-list.sort >.checkbox')
    TEXT_WHEN_NO_PRODUCTS = (By.CSS_SELECTOR, '.content-products h2')


class ProductPage(BasePage):

    def __init__(self, context):
        BasePage.__init__(self, context.browser)

    def click_button_product(self):
        self.find_elements(*ProductPageLocators.BUTTON_PRODUCT_MAINPAGE)[0].click()

    def get_window_title(self):
        WebDriverWait(self.browser, self.timeout).until(EC.visibility_of_element_located((By.CSS_SELECTOR, "#add_product_form")))
        return self.find_element(*ProductPageLocators.WINDOW_ADD_PRODUCT).text

    def enter_product_name(self, name):
        self.find_element(*ProductPageLocators.INPUT_PRODUCT_NAME).clear()
        self.find_element(*ProductPageLocators.INPUT_PRODUCT_NAME).send_keys(name)

    def enter_product_price(self, price):
        self.find_element(*ProductPageLocators.INPUT_PRODUCT_PRICE).clear()
        self.find_element(*ProductPageLocators.INPUT_PRODUCT_PRICE).send_keys(price)

    def enter_catalog_number(self, number):
        self.find_element(*ProductPageLocators.INPUT_CATALOG_NUMBER).clear()
        self.find_element(*ProductPageLocators.INPUT_CATALOG_NUMBER).send_keys(number)

    def enter_required_fields(self, name, number, price):
        self.enter_product_name(name)
        self.enter_catalog_number(number)
        self.enter_product_price(price)

    def click_button_save(self):
        button = self.browser.find_element_by_css_selector('#add_product_form')
        button.find_element_by_css_selector('.btn.btn-large.btn-blue').click()

    def get_text_notification_success(self):
        return self.find_element(*ProductPageLocators.NOTIFICATION_SUCCESS).text

    def click_back_to_product_list(self):
        return self.find_element(*ProductPageLocators.BUTTON_BACK_TO_PRODUCTS_LIST).click()

    def click_select_produckt(self):
        WebDriverWait(self.browser, self.timeout).until(EC.visibility_of_element_located((By.CSS_SELECTOR, '.items-list.sort')))
        self.browser.find_elements_by_css_selector('.items-list.sort td.checkbox')[0].click()

    def click_dropdown_action(self, num):
        WebDriverWait(self.browser, self.timeout).until(EC.visibility_of_element_located((By.CSS_SELECTOR, '.actions-bar.active')))
        self.find_element(*ProductPageLocators.DROPDOWN_ACTIONS).click()
        self.click_dropdown_list(num)

    def click_dropdown_list(self, num):
        ele = self.browser.find_element_by_css_selector('.btn-group.bootstrap-select.to-bottom.dropup.action-select.show')
        ele.find_elements_by_css_selector('div[class="dropdown-menu show"] li')[num].click()

    def click_button_save_action(self):
        self.find_element(*ProductPageLocators.BUTTON_SAVE_ACTION).click()

    def get_text_when_no_products(self):
        WebDriverWait(self.browser, self.timeout).until(EC.visibility_of_element_located(((By.CSS_SELECTOR, '.blank-slate-icon'))))
        return self.find_element(*ProductPageLocators.TEXT_WHEN_NO_PRODUCTS).text












