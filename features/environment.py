
from selenium import webdriver
from features.pages.login_page import BasePage


def before_feature(context, scenario):
    print("\nScenario start")
    print("User data:", context.config.userdata)

    if 'browser' in context.config.userdata.keys():
        if context.config.userdata['browser'] is None:
            BROWSER = 'chrome'
        else:
            BROWSER = context.config.userdata['browser']
    else:
        BROWSER = 'chrome'
    if BROWSER == 'chrome':
        context.browser = webdriver.Chrome()
    elif BROWSER == 'firefox':
        context.browser = webdriver.Firefox()
    else:
        print("Browser you entered:", BROWSER, "is invalid value")

    context.browser.maximize_window()
    context.page = BasePage(context)
    # context.page.login(context.config.userdata['user'], context.config.userdata['password'], context.config.userdata['url'])


def after_feature(context, scenario):
    #print("\nScenario status " + scenario.status)
    # context.page = LoginPage(context)
    # context.page.logout()
    context.browser.quit()




