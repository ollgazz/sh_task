# QA task
This repository is part of QA task

# Usage  
1. If you do not know how to use git, click "Download zip" option at right corner  
2. For Git users, git clone  
3. Install Python 3.6
4. Install requirements.txt 

# Run tests
1. Open a shell/command prompt and from the root folder run:
   behave features --no-capture 
   
# Modify the data for tests:
    i.e behave features --no-capture  -D user="username" -D pass="password" -D phone="404909707"
   
   ALL THE VARIABLES ARE ADDITIONAL :  <i>All the variables are store in file behave.ini</i><br>
     
   -> "browser" - without adding that value the chrome browser will be used values which can be passed: "firefox" "chrome"<br>
   -> "mail" <br>
   -> "user" <br>
   -> "phone" <br>
   -> "store_name" <br>
   -> "pass" <br>
   -> "register_url" <br>
   -> "name_product" <br>
   -> "sku" <br>
   -> "price" <br>
   -> "invalid_mail" <br>
   -> "invalid_pass" <br>
